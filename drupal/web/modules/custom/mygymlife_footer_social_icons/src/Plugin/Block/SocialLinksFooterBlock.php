<?php

namespace Drupal\mygymlife_footer_social_icons\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 *  Block that renders footer social links based on page section.
 * @Block(
 *  id = "mygymlife_footer_social_icons",
 *  admin_label = @Translation("Footer social links"),
 * )
 */

class SocialLinksFooterBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {

    $socials = '<ul class="social-icon">';
    $socials .= '<li class="social-icon__item social-icon__item--facebook"><a href="#"></a></li>';
    $socials .= '<li class="social-icon__item social-icon__item--google"><a href="#"></a></li>';
    $socials .= '<li class="social-icon__item social-icon__item--twitter"><a href="#"></a></li>';
    $socials .= '<li class="social-icon__item social-icon__item--pinterest"><a href="#"></a></li>';
    $socials .= '</ul>';

    return [
      '#markup' => $socials,
    ];
  }

}