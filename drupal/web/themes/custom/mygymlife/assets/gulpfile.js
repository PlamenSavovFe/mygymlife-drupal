'use strict';
var gulp = require('gulp');
var sass = require('gulp-sass');
var autoprefixer = require('gulp-autoprefixer');

// Variables.
var scssPath = 'src/scss';

// Compile.
gulp.task('scss:build', function () {
  return gulp.src(scssPath + '/*.scss')
    .pipe(sass().on('error', sass.logError))
    .pipe(autoprefixer({
			overrideBrowserslist: ['last 4 versions'],
			cascade: false
		}))
    .pipe(gulp.dest('dist/css'));
});

// Compile and watch.
gulp.task('scss:watch', function () {
  return gulp.watch(scssPath + '/**/*.scss', gulp.series('scss:build'));
});

