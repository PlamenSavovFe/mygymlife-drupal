// Main javascript file.
//


(function($) {

  // Use strict mode to avoid errors:
  // https://developer.mozilla.org/en/JavaScript/Strict_mode
  "use strict";
  Drupal = Drupal || {};
  Drupal.ajax = Drupal.ajax || {};
  Drupal.ajax.prototype = Drupal.ajax.prototype || {};
  Drupal.ajax.prototype.commands = Drupal.ajax.prototype.commands || {};

  const VIEWPORT_L = 941;


  // Mobile main navigation - show menu after click on sandwich icon.
  Drupal.behaviors.mainNavigation = {
    attach: function (context, setting) {
      $('[data-navbar-menu-open]', context).click(function(e){
        $(this).toggleClass('navbar-icon--active');
        $('.navigation__menu-wrapper').toggleClass('navigation__menu--show');
      });
    }
  }
  
  // Mobile menu dropdown
  Drupal.behaviors.mainNavigationDropdown = {
    attach: function (context, setting) {
      if( $(window).width() < VIEWPORT_L ) {
        $('.navigation__dropdown', context).click(function(e){
          e.preventDefault();
          $(this).closest('.navigation__dropdown').toggleClass('navigation__dropdown--active');
          $(this).closest('.navigation__dropdown').find('.navigation__menu-sub').toggleClass('navigation__dropdown--show');
        });
  
        $('.navigation__menu-sub li a', context).click(function(e) {
          e.stopPropagation();
        });
      }
    }
  }

  // Sticky header.
  Drupal.behaviors.mainNavigationScroll = {
    attach: function (context, setting) {
      if ($('[data-header-fixed]').length > 0) {
        $('body').addClass('body--navigation-fixed');
      }

      $(window, context).scroll(function(e) {
        if( $(window).width() > VIEWPORT_L ) {
          if ($(window).scrollTop() > 1) {
            $('[data-header-fixed]').addClass('sticky-header');
          } else {
            $('[data-header-fixed]').removeClass('sticky-header');
          }
        }
      });
    }
  }

  // Add slick slider to Header slider.
  Drupal.behaviors.carousel = {
    attach: function (context, setting) {
      $('.header__slider', context).slick({
        infinite: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 6000,
        speed: 1000,
        arrows: true,
        dots: false,
        responsive: [
          {
            breakpoint: 941,
            settings: {
              arrows: false,
              dots: true
            }
          }
        ]
      });
    }
  }

  // Countdown.
  Drupal.behaviors.Coutdown = {
    attach: function (context, setting) {
      setInterval(function time(e){
        var d = new Date();
        var hours = 23 - d.getHours() ;
        var min = 59 - d.getMinutes();

        if((min + '').length == 1){
          min = '0' + min;
        }

        var sec = 59 - d.getSeconds();

        if((sec + '').length == 1){
          sec = '0' + sec;
        }

        $('.countdown__timer').html('<div class="countdown__timer-section">'+hours+'<span>'+Drupal.t("hrs")+'</span></div>'+'<div class="countdown__timer-section">'+min+'<span>'+Drupal.t("mins")+'</span></div>'+'<div class="countdown__timer-section">'+sec+'<span>'+Drupal.t("secs")+'</span></div>')
      }, 1000);
    }
  }
})(jQuery);